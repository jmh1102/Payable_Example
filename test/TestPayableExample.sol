pragma solidity ^0.4.19;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/PayableExample.sol";

contract TestPayableExample {
  uint public initialBalance = 1 wei;

  function testDeposit() external payable {
    PayableExample obj = new PayableExample();

    obj.deposit.value(1)();

    Assert.equal(obj.myBalance(), initialBalance, "#myBalance() should returns 1");
  }

}