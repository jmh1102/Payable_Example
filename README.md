# Test Payable Function via Truffle

## 0.install truffle & ganache-cli
    npm install -g truffle
    npm install -g ganache-cli

## 1.clone the repo
    git clone

## 2.run ethereum client(using ganache, port:8545)
    ganache-cli

## 3.run Solidity test
    truffle test ./test/TestPayableExample.sol
